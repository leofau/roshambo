package roshambo.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import roshambo.model.Move;

import static roshambo.model.Move.Type.*;


/**
 * Initialize all possible moves
 */
@Configuration
public class MoveFactoryBeanAppConfig {

    @Bean(name = "scissorsMove")
    public Move scissorsFactory() {
        return new Move(SCISSORS, PAPER);
    }

    @Bean(name = "stoneMove")
    public Move stoneFactory() {
        return new Move(STONE, SCISSORS);
    }

    @Bean(name = "paperMove")
    public Move paperFactory() {
        return new Move(PAPER, STONE, WELL);
    }

    @Bean(name = "wellMove")
    public Move wellFactory() {
        return new Move(WELL, STONE, SCISSORS);
    }

}
