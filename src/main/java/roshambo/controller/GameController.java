package roshambo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import roshambo.model.GameResult;
import roshambo.model.Move;
import roshambo.services.interfaces.GameServiceInterface;
import roshambo.services.interfaces.OpponentAIServiceInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class GameController {

    @Autowired
    private GameServiceInterface gameService;

    @Autowired
    private OpponentAIServiceInterface opponent;

    private Map<Move.Type, Move> moves;
    @Autowired
    public GameController(List<Move> availableMoves) {
        moves = new HashMap<>();

        for (Move move : availableMoves) {
            moves.put(move.getType(), move);
        }
    }

    @RequestMapping(value = "/move", method = RequestMethod.POST)
    public GameResult move(@RequestBody Map<String, Move.Type> moveParams) {
        Assert.isTrue(moveParams.size() == 1, "more params than allowed");
        Assert.isTrue(moveParams.containsKey("type"), "type missing");

        return gameService.getGameResult(moves.get(moveParams.get("type")), opponent.move());
    }

    @RequestMapping(value = "/moves", method = RequestMethod.GET)
    public ResponseEntity<List<Move>> moves() {
        return new ResponseEntity<>(new ArrayList<>(moves.values()), HttpStatus.OK);
    }
}
