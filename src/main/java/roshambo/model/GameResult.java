package roshambo.model;

import lombok.Data;

@Data
public class GameResult {

    /**
     * All Possible Results
     */
    public enum Result {WIN, LOSE, DRAW}

    /**
     * move of player A
     */
    private Move moveA;

    /**
     * move of player B
     */
    private Move moveB;

    /**
     * result in the viewpoint of player A
     */
    private Result result;
}
