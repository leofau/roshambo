package roshambo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
public class Move {

    /**
     * all possible move types
     */
    public enum Type {
        SCISSORS, STONE, PAPER, WELL
    }

    /**
     * all moves this one wins against
     */
    @JsonIgnore
    private List<Type> winsAgainst;

    private Type type;

    public Move(Type type, Type... winsAgainst) {
        this.winsAgainst = new ArrayList<>(Arrays.asList(winsAgainst));
        this.type = type;
    }

    /**
     * Add moves to win against
     *
     * @param moves to add
     */
    public void addWinsAgainst(Type... moves) {
        this.winsAgainst.addAll(Arrays.asList(moves));
    }

    /**
     * @param move to check against
     * @return true if winner, false if looser oder draw
     */
    public boolean winsAgainst(Type move) {
        return winsAgainst.contains(move);
    }
}
