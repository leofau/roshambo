package roshambo.services;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import roshambo.model.GameResult;
import roshambo.model.Move;
import roshambo.services.interfaces.GameServiceInterface;

@Service("gameService")
public class GameService implements GameServiceInterface {
    @Override
    public GameResult getGameResult(Move moveA, Move moveB) {
        Assert.notNull(moveA, "Move A missing");
        Assert.notNull(moveB, "Move B missing");

        GameResult result = new GameResult();
        result.setMoveA(moveA);
        result.setMoveB(moveB);

        Move.Type typeA = moveA.getType();
        Move.Type typeB = moveB.getType();

        if (moveA.winsAgainst(typeB) && !moveB.winsAgainst(typeA)) {
            result.setResult(GameResult.Result.WIN);
        } else if (!moveA.winsAgainst(typeB) && moveB.winsAgainst(typeA)) {
            result.setResult(GameResult.Result.LOSE);
        } else {
            result.setResult(GameResult.Result.DRAW);
        }
        return result;
    }
}
