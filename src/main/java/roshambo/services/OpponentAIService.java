package roshambo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import roshambo.model.Move;
import roshambo.services.interfaces.OpponentAIServiceInterface;

import java.util.List;
import java.util.Random;

@Service("opponentAIService")
public class OpponentAIService implements OpponentAIServiceInterface {

    private Random random;

    @Autowired
    private List<Move> moves;

    public OpponentAIService() {
        random = new Random();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Move move() {
        return moves.get(random.nextInt(moves.size()));
    }
}
