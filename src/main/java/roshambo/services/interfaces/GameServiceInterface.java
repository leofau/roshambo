package roshambo.services.interfaces;

import roshambo.model.GameResult;
import roshambo.model.Move;

public interface GameServiceInterface {

    /**
     * Returns the game result in the view of moveA
     * If the moves are incomplete or invalid (e.g. both wins against each other). The result is a draw.
     *
     * @param moveA move of player A
     * @param moveB move of player B
     * @return the result of the game
     */
    GameResult getGameResult(Move moveA, Move moveB);

}
