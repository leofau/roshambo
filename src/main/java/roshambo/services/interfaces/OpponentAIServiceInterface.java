package roshambo.services.interfaces;

import roshambo.model.Move;

public interface OpponentAIServiceInterface {

    /**
     * chose a random move
     *
     * @return move of computer opponent
     */
    Move move();
}
