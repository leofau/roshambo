package roshambo.functional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.isIn;
import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class GameRestTest {
    private List<String> results;
    private List<String> moves;

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setUp() {
        results = new ArrayList<>(Arrays.asList("DRAW", "WIN", "LOSE"));
        moves = new ArrayList<>(Arrays.asList("WELL", "PAPER", "STONE", "SCISSORS"));
    }

    @Test
    public void testGetMoves() throws Exception {

        ResultActions resultActions = mockMvc.perform(get("/moves"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
        for (String move: moves) {
                resultActions.andExpect(content().string(containsString(move)));
        }
    }

    @Test
    public void testBadRequest() throws Exception {
        mockMvc.perform(post("/move")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"type\":\"TEST\"}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testOK() throws Exception {
        for (String move : moves) {
            this.postTest(mockMvc, move);
        }
    }

    private void postTest(MockMvc mockMvc, String moveA) throws Exception {
        mockMvc.perform(post("/move")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"type\":\"" + moveA + "\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("moveA.type").isString())
                .andExpect(jsonPath("moveA.type").value(moveA))
                .andExpect(jsonPath("moveB.type").isString())
                .andExpect(jsonPath("moveB.type").value(isIn(moves)))
                .andExpect(jsonPath("result").isString())
                .andExpect(jsonPath("result").value(isIn(results)));

    }
}
