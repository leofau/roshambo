package roshambo.unit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import roshambo.configuration.MoveFactoryBeanAppConfig;
import roshambo.model.Move;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static roshambo.model.Move.Type.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = MoveFactoryBeanAppConfig.class)
public class FactoryBeanTest {

    @Autowired
    private List<Move> moves;

    @Test
    public void testConfiguration() {
        assertThat(moves.size(), equalTo(4));
        assertThat(
                moves,
                containsInAnyOrder(
                        new Move(SCISSORS, PAPER),
                        new Move(STONE, SCISSORS),
                        new Move(PAPER, STONE, WELL),
                        new Move(WELL, STONE, SCISSORS)
                ));
    }


}
