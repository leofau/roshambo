package roshambo.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import roshambo.model.GameResult;
import roshambo.model.Move;
import roshambo.services.interfaces.GameServiceInterface;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static roshambo.model.Move.Type.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GameServiceTest {

    @Autowired
    private GameServiceInterface gameService;

    @Autowired
    private List<Move> movesList;

    private Map<Move.Type, Move> moves;

    @Before
    public void setUp() {
        moves = new HashMap<>();
        for (Move move : movesList) {
            moves.put(move.getType(), move);
        }
    }

    @Test
    public void testMovesInit() {
        assertThat(moves.size(), equalTo(4));
        assertThat(movesList.size(), equalTo(4));
    }

    @Test
    public void testWin() {
        GameResult result = new GameResult();
        result.setResult(GameResult.Result.WIN);

        result.setMoveA(moves.get(SCISSORS));
        result.setMoveB(moves.get(PAPER));
        assertEquals(result, gameService.getGameResult(moves.get(SCISSORS), moves.get(PAPER)));

        result.setMoveA(moves.get(PAPER));
        result.setMoveB(moves.get(STONE));
        assertEquals(result, gameService.getGameResult(moves.get(PAPER), moves.get(STONE)));


        result.setMoveA(moves.get(STONE));
        result.setMoveB(moves.get(SCISSORS));
        assertEquals(result, gameService.getGameResult(moves.get(STONE), moves.get(SCISSORS)));
    }

    @Test
    public void testLose() {
        GameResult result = new GameResult();
        result.setResult(GameResult.Result.LOSE);

        result.setMoveA(moves.get(PAPER));
        result.setMoveB(moves.get(SCISSORS));
        assertEquals(result, gameService.getGameResult(moves.get(PAPER), moves.get(SCISSORS)));

        result.setMoveA(moves.get(STONE));
        result.setMoveB(moves.get(PAPER));
        assertEquals(result, gameService.getGameResult(moves.get(STONE), moves.get(PAPER)));


        result.setMoveA(moves.get(SCISSORS));
        result.setMoveB(moves.get(STONE));
        assertEquals(result, gameService.getGameResult(moves.get(SCISSORS), moves.get(STONE)));
    }

    @Test
    public void testDraw() {
        GameResult result = new GameResult();
        result.setResult(GameResult.Result.DRAW);

        result.setMoveA(moves.get(PAPER));
        result.setMoveB(moves.get(PAPER));
        assertEquals(result, gameService.getGameResult(moves.get(PAPER), moves.get(PAPER)));

        result.setMoveA(moves.get(STONE));
        result.setMoveB(moves.get(STONE));
        assertEquals(result, gameService.getGameResult(moves.get(STONE), moves.get(STONE)));


        result.setMoveA(moves.get(SCISSORS));
        result.setMoveB(moves.get(SCISSORS));
        assertEquals(result, gameService.getGameResult(moves.get(SCISSORS), moves.get(SCISSORS)));
    }

    @Test
    public void testWell() {
        moves.get(PAPER).addWinsAgainst(WELL);

        GameResult result = new GameResult();

        result.setResult(GameResult.Result.WIN);
        result.setMoveA(moves.get(WELL));
        result.setMoveB(moves.get(STONE));
        assertEquals(result, gameService.getGameResult(moves.get(WELL), moves.get(STONE)));

        result.setMoveB(moves.get(SCISSORS));
        assertEquals(result, gameService.getGameResult(moves.get(WELL), moves.get(SCISSORS)));

        result.setMoveA(moves.get(PAPER));
        result.setMoveB(moves.get(WELL));
        assertEquals(result, gameService.getGameResult(moves.get(PAPER), moves.get(WELL)));

        result.setResult(GameResult.Result.LOSE);
        result.setMoveA(moves.get(WELL));
        result.setMoveB(moves.get(PAPER));
        assertEquals(result, gameService.getGameResult(moves.get(WELL), moves.get(PAPER)));
        result.setMoveA(moves.get(SCISSORS));
        result.setMoveB(moves.get(WELL));
        assertEquals(result, gameService.getGameResult(moves.get(SCISSORS), moves.get(WELL)));
        result.setMoveA(moves.get(STONE));
        result.setMoveB(moves.get(WELL));
        assertEquals(result, gameService.getGameResult(moves.get(STONE), moves.get(WELL)));

        result.setResult(GameResult.Result.DRAW);
        result.setMoveA(moves.get(WELL));
        result.setMoveB(moves.get(WELL));
        assertEquals(result, gameService.getGameResult(moves.get(WELL), moves.get(WELL)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testANull() {
        gameService.getGameResult(null, moves.get(STONE));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBNull() {
        gameService.getGameResult(moves.get(STONE), null);
    }
}
