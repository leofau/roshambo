package roshambo.unit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import roshambo.model.GameResult;
import roshambo.model.Move;
import roshambo.services.interfaces.GameServiceInterface;

import static org.hamcrest.CoreMatchers.*;


import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PlausibilityCheckTest {

    @Autowired
    private List<Move> movesList;

    @Autowired
    private GameServiceInterface gameService;

    @Test
    public void testPlausibility() {
        GameResult result = new GameResult();
        result.setResult(GameResult.Result.DRAW);

        //Check that only equal moves result in a draw
        for (Move moveA : movesList) {
            result.setMoveA(moveA);
            for (Move moveB : movesList) {
                result.setMoveB(moveB);
                if (moveA.equals(moveB)) {
                    assertEquals(result, gameService.getGameResult(moveA, moveB));
                } else {
                    assertThat(moveA + " against " + moveB + "mussn't be draw", gameService.getGameResult(moveA, moveB).getResult(), not(is(result.getResult())));
                }
            }
        }
    }
}
